from django.apps import AppConfig


class AdonisSnsConfig(AppConfig):
    name = 'adonis_sns'
